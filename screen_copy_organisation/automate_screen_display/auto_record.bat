@echo off
echo Please connect the Oculus device via the usb cable and make sure it's switched on.
pause
for /f "tokens=9" %%a in ('adb shell ip route') do (echo IP of Oculus GO: %%a&set ipaddr=%%a)

echo.
echo Changing to TCPIP port 5555
adb tcpip 5555
echo.

echo Please disconnect the usb cable and make sure the Oculus device is switched on.
pause
adb connect %ipaddr%:5555

echo adb is now connected wirelessly to your Oculus device
pause  

echo Bringing the Left eye to the foreground
scrcpy -c 1440:1600:0:0